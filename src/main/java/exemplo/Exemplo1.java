package exemplo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import domain.Administrativo;
import domain.Cidade;
import domain.Endereco;
import domain.Estado;
import domain.Funcionario;
import domain.PessoaJuridica;
import domain.RamoAtividade;
import domain.Vendedor;
import enums.SituacaoVendedorEnum;



public class Exemplo1 {

	public static void main(String[] args)  {

		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("varejista");
			EntityManager em = emf.createEntityManager();
			em.getTransaction().begin();
		

			// Estado estadoBA = new Estado("BA", "Bahia");
			// Cidade cidadeSSA = new Cidade("SSA", "Salvador", estadoBA);
			// em.persist(estadoBA);
			// em.persist(cidadeSSA);

			
			Estado estadoBA = new Estado("BA", "Bahia");
			Estado estadoSP = new Estado("SP", "Sao Paulo");
			Estado estadoRJ = new Estado("RJ", "Rio de Janeiro");
			em.persist(estadoRJ);
			em.persist(estadoSP);
			em.persist(estadoBA);
			
			Cidade cidadeSP = new Cidade("SP", "Sao Paulo", estadoSP);
			Cidade cidadeCP = new Cidade("CP", "Campinas", estadoSP);
			Cidade cidadeSSA = new Cidade("SSA", "Salvador", estadoBA);
			Cidade cidadeLF = new Cidade("LF", "Lauro de Firetas", estadoBA);
			Cidade cidadeSF = new Cidade("SF", "Simoes Filho", estadoBA);
			
			
			em.persist(cidadeSP);
			em.persist(cidadeCP);
			em.persist(cidadeSSA);
			em.persist(cidadeSF);
			em.persist(cidadeLF);
			
			Endereco endereco1 = new Endereco("Rua das aboboras","Pituba", cidadeSSA);
			Endereco endereco2 = new Endereco("Rua das margaridas","Brotas", cidadeSSA);
			Endereco endereco3 = new Endereco("Rua das cobras","Judas", cidadeSP);
			
			RamoAtividade ramo1 = new RamoAtividade("Construcao Civil");
			em.persist(ramo1);
			RamoAtividade ramo2 = new RamoAtividade("Advocacia");
			em.persist(ramo2);
			RamoAtividade ramo3 = new RamoAtividade("Vendas");
			em.persist(ramo3);
			
			List <String> telefone1 = new ArrayList();
			telefone1.add("3379-5510");
			telefone1.add("8881-8190");
			
			List <String> telefone2 = new ArrayList();
			telefone1.add("3369-6611");
			telefone1.add("9471-9134");
			
			Date data1 = new Date("15/04/1998");
			Date data2 = new Date("10/01/1992");
			Date data3 = new Date("12/05/2000");
			Date data4 = new Date("19/02/2001");
			
			/*List <PessoaJuridica> pj = new ArrayList();
			pj.add()
			*/
			
			List <RamoAtividade> ramolist = new ArrayList();
			ramolist.add(ramo1);
			ramolist.add(ramo2);
			ramolist.add(ramo3);
			List <PessoaJuridica> pjlist = new ArrayList();
			Vendedor func1 = new Vendedor("07712781245", "Rafael", "541174567", "SSPBA", "BA", telefone1, data2, endereco2, new BigDecimal(0.25), SituacaoVendedorEnum.ATIVO, pjlist);
			Vendedor func2 = new Vendedor("43113541319", "Saulo", "913426870", "SSPSP", "SP", telefone1, data3, endereco3, new BigDecimal(0.20), SituacaoVendedorEnum.ATIVO, pjlist);
			Vendedor func3 = new Vendedor("12312781245", "Joseph", "415565454", "SSPBA", "BA", telefone1, data4, endereco2, new BigDecimal(0.35), SituacaoVendedorEnum.ATIVO, pjlist);
			Vendedor func4 = new Vendedor("43212799711", "Fred", "751344567", "SSPBA", "BA", telefone1, data4, endereco1, new BigDecimal(0.25), SituacaoVendedorEnum.ATIVO, pjlist);
			Vendedor func5 = new Vendedor("51113541319", "Rainik", "913464210", "SSPSP", "SP", telefone1, data3, endereco3, new BigDecimal(0.25), SituacaoVendedorEnum.ATIVO, pjlist);
			
			BigDecimal b1 = new BigDecimal(500);
			
			List <Vendedor> vendedorlist = new ArrayList();
			vendedorlist.add(func1);
			vendedorlist.add(func2);
			vendedorlist.add(func3);
			vendedorlist.add(func4);
			vendedorlist.add(func5);
			
			em.persist(func1);
			em.persist(func2);
			em.persist(func3);
			em.persist(func4);
			em.persist(func5);
			
					
					
			PessoaJuridica pj1 = new PessoaJuridica("123456789", "semsaco", ramolist, new BigDecimal(5000), vendedorlist);
			PessoaJuridica pj2 = new PessoaJuridica("987654321", "odeiobanco", ramolist, new BigDecimal(2000), vendedorlist);
			PessoaJuridica pj3 = new PessoaJuridica("111222333", "quesaco", ramolist, new BigDecimal(4000), vendedorlist);
			PessoaJuridica pj4 = new PessoaJuridica("333222111", "naguentomais", ramolist, new BigDecimal(3000), vendedorlist);
			PessoaJuridica pj5 = new PessoaJuridica("777888999", "lulapreso", ramolist, new BigDecimal(1000), vendedorlist);
			
			em.persist(pj1);
			em.persist(pj2);
			em.persist(pj3);
			em.persist(pj4);
			em.persist(pj5);
			
	
			
		
			
			Administrativo func6 = new Administrativo("976431258", "suamae", "123", "SSPBA", "BA",telefone1,  data1, endereco1, 1);
			Administrativo func7 = new Administrativo("444555777", "sauloxe", "321", "SSPBA", "BA",telefone1,  data1, endereco1, 1);
			Administrativo func8 = new Administrativo("333777999", "anteny", "222", "SSPBA", "BA",telefone1,  data1, endereco1, 1);
			em.persist(func6);
			em.persist(func7);
			em.persist(func8);
			
			
			em.getTransaction().commit();
			
			
			//CONSULTA C
			TypedQuery<Vendedor> query = em.createQuery("select v from tab_vendedor v where v.situacao = :situacao",Vendedor.class);
			query.setParameter("situacao", SituacaoVendedorEnum.ATIVO);
			List<Vendedor> vendedores = query.getResultList();
			System.out.println("ATIVOS: ");
			for (Vendedor vendedor : vendedores) {		
				System.out.println(vendedor.getCpf() + "-" + vendedor.getNome());
			}
			//CONSULTA D
			TypedQuery<Estado> query2 = em.createQuery("select e from tab_estado e where e.sigla not in (select c.estado.sigla from tab_cidade c)", Estado.class);
			List<Estado> estados = query2.getResultList();
			System.out.println("Estados sem cidade:");
			for (Estado estado : estados) {
				System.out.println(estado.getSigla() + " - " + estado.getNome());
			}
//			//CONSULTA E
//			TypedQuery<Vendedor> query3 = em.createQuery("select v.endereco.cidade.nome, count(*) from tab_vendedor v group by v.endereco.cidade.nome",Vendedor.class);
//			List<Vendedor> vendedores2 = query3.getResultList();
//			System.out.println("Vendedores cidade:");
//			for (Vendedor vendedor : vendedores2) {
//				System.out.println("XD");
//			}
			//CONSULTA F
			String cidade = "Salvador";
			TypedQuery<Vendedor> query5 = em.createQuery("select v from tab_vendedor v where v.endereco.cidade.nome = :cidade", Vendedor.class);
			query5.setParameter("cidade", cidade);
			List<Vendedor> vendedores3 = query5.getResultList();
			System.out.println("Vendedores de " + cidade + ":");
			for (Vendedor vendedor : vendedores3) {
				System.out.println(vendedor.getNome() + " - " + vendedor.getTelefones());
			}
			//CONSULTA B
			String ramo = "Vendas";
			TypedQuery<Vendedor> query6 = em.createQuery("select distinct v from tab_vendedor v inner join v.clientes c inner join c.ramosAtividade r where r.nome = :ramo order by v.nome",Vendedor.class);
			query6.setParameter("ramo", ramo);
			List<Vendedor> vendedores4 = query6.getResultList();
			System.out.println("Vendedores do ramo de " + ramo + ":");
			for (Vendedor vendedor : vendedores4) {
				System.out.println(vendedor.getNome() + " - " + vendedor.getTelefones() + " - " + vendedor.getEndereco());
			}
			//CONSULTA A
			String vendedor = "Rafael";
			TypedQuery<PessoaJuridica> query7 = em.createQuery("select c from tab_vendedor v inner join v.clientes c where v.nome = :vendedor", PessoaJuridica.class);
			query7.setParameter("vendedor", vendedor);
			List<PessoaJuridica> clientes = query7.getResultList();
			System.out.println("Clientes de " + vendedor + ":" );
			for (PessoaJuridica cliente : clientes) {
				System.out.println("Nome:" + cliente.getNome());
			}
		} catch (Exception e) {
			logger.error("Erro: ", e);
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
		
	}
	
	
	
	private static final Logger logger = Logger.getLogger(Exemplo1.class);

}

