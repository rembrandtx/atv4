package domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

import converter.Converter;
import enums.SituacaoVendedorEnum;



@Entity(name = "tab_vendedor")

// quando strategy=InheritanceType.SINGLE_TABLE)
// @DiscriminatorValue("VND")
public class Vendedor extends Funcionario {

	@Column(name = "percentual_comissao", precision = 10, scale = 2, nullable = false)
	private BigDecimal percentualComissao;// - numeric(10,2) - not null

	// @Enumerated(EnumType.STRING)
	@Convert(converter = Converter.class)
	@Column(columnDefinition = "char(3)", nullable = false)
	private SituacaoVendedorEnum situacao;// - char(3) - not null

	@ManyToMany(mappedBy = "vendedores")
	private List<PessoaJuridica> clientes;//

	public Vendedor() {
	}

	public Vendedor(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf, List<String> telefones,
			Date dataNascimento, Endereco endereco, BigDecimal percentualComissao, SituacaoVendedorEnum situacao,
			List<PessoaJuridica> clientes) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, dataNascimento, endereco);
		this.percentualComissao = percentualComissao;
		this.situacao = situacao;
		this.clientes = clientes;
	}

	public BigDecimal getPercentualComissao() {
		return percentualComissao;
	}

	public void setPercentualComissao(BigDecimal percentualComissao) {
		this.percentualComissao = percentualComissao;
	}

	public SituacaoVendedorEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoVendedorEnum situacao) {
		this.situacao = situacao;
	}

	public List<PessoaJuridica> getClientes() {
		return clientes;
	}

	public void setClientes(List<PessoaJuridica> clientes) {
		this.clientes = clientes;
	}

	@Override
	public String toString() {
		return "Vendedor [percentualComissao=" + percentualComissao + ", situacao=" + situacao + ", clientes="
				+ clientes + ", toString()=" + super.toString() + "]";
	}

}
