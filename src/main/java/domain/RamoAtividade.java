package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "tab_ramo_atividade")
public class RamoAtividade {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(nullable = false)
	private Integer id;// - integer (sequence) - not null

	@Column(length = 40, nullable = false)
	private String nome;// - varchar(40) - not null

	public RamoAtividade() {
	}

	public RamoAtividade(String nome) {
		super();
		this.nome = nome;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "RamoAtividade [id=" + id + ", nome=" + nome + "]";
	}

}
