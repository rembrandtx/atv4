package domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity(name = "tab_pessoa_juridica")
public class PessoaJuridica {

	@Id
	@Column(columnDefinition = "char(11)", nullable = false)
	private String cnpj;// - char(11) - not null

	@Column(length = 40, nullable = false)
	private String nome;// - varchar(40) - not null

	@ManyToMany
	private List<RamoAtividade> ramosAtividade;//

	@Column(precision = 10, scale = 2, nullable = false)
	private BigDecimal faturamento;// - numrica(10,2) - not null

	@ManyToMany
	private List<Vendedor> vendedores;//

	public PessoaJuridica() {
	}
	
	public PessoaJuridica(String cnpj, String nome, List<RamoAtividade> ramosAtividade, BigDecimal faturamento,
			List<Vendedor> vendedores) {
		super();
		this.cnpj = cnpj;
		this.nome = nome;
		this.ramosAtividade = ramosAtividade;
		this.faturamento = faturamento;
		this.vendedores = vendedores;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<RamoAtividade> getRamosAtividade() {
		return ramosAtividade;
	}

	public void setRamosAtividade(List<RamoAtividade> ramosAtividade) {
		this.ramosAtividade = ramosAtividade;
	}

	public BigDecimal getFaturamento() {
		return faturamento;
	}

	public void setFaturamento(BigDecimal faturamento) {
		this.faturamento = faturamento;
	}

	public List<Vendedor> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}

	@Override
	public String toString() {
		return "PessoaJuridica [cnpj=" + cnpj + ", nome=" + nome + ", ramosAtividade=" + ramosAtividade
				+ ", faturamento=" + faturamento + ", vendedores=" + vendedores + "]";
	}

}
