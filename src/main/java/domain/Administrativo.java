package domain;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="tab_administrativo")

//quando strategy=InheritanceType.SINGLE_TABLE)
//@DiscriminatorValue("ADM")
public class Administrativo extends Funcionario {

	@Column( nullable = false)
	private Integer turno;

	public Administrativo() {
	}
	
	public Administrativo(String cpf, String nome, String rg, String rgOrgaoExpedidor, String rgUf,
			List<String> telefones, java.util.Date data1, Endereco endereco, Integer turno) {
		super(cpf, nome, rg, rgOrgaoExpedidor, rgUf, telefones, data1, endereco);
		this.turno = turno;
	}

	public Integer getTurno() {
		return turno;
	}

	public void setTurno(Integer turno) {
		this.turno = turno;
	}

	@Override
	public String toString() {
		return "Administrativo [turno=" + turno + ", toString()=" + super.toString() + "]";
	}

}
