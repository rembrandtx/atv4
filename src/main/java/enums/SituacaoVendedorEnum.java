package enums;

public enum SituacaoVendedorEnum {

	ATIVO("ATV"), SUSPENSO("SPN");

	private String codigo;

	private SituacaoVendedorEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static SituacaoVendedorEnum valueOfCodigo(String codigo) {
		for (SituacaoVendedorEnum situacaoVendedorEnum : values()) {
			if (situacaoVendedorEnum.getCodigo().equalsIgnoreCase(codigo)) {
				return situacaoVendedorEnum;
			}
		}
		throw new IllegalArgumentException("Codigo no vlido: " + codigo);
	}

}
