package converter;

import javax.persistence.AttributeConverter;

import enums.SituacaoVendedorEnum;

public class Converter implements AttributeConverter<SituacaoVendedorEnum, String> {

	public String convertToDatabaseColumn(SituacaoVendedorEnum situacaoVendedorEnum) {
		return situacaoVendedorEnum != null ? situacaoVendedorEnum.getCodigo() : null;
	}

	public SituacaoVendedorEnum convertToEntityAttribute(String dbData) {
		return dbData != null ? SituacaoVendedorEnum.valueOfCodigo(dbData) : null;
	}

}
